const getSlots = require('../../../../src/domain/event/logistic/getSlots')
const slotRepository = require('../../../../src/infraestructure/repository/logistic/slotRepository')
const toggle = require('../../../../src/infraestructure/toggles')

const sinon = require('sinon')

require('dotenv').config()

afterEach(() => {
    sinon.restore()
})

describe('getSlots unit test', () => {
    test('should return slots available from logistic', async () => {
      
        sinon.stub(slotRepository, 'get').returns([{ id: 1 },  { id: 2 }]);

        const slots  = await getSlots.getSlotsAvailableFromLogistic(); 

        expect(slots).toEqual([{ id: 1 }, { id: 2 }]);


    });
});


