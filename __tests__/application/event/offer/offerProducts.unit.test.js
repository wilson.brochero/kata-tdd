const getProducts = require ('../../../../src/domain/event/logistic/getProducts')
const offerProducts = require('../../../../src/application/event/offer/offerProducts')

const sinon = require('sinon')

afterEach(() => {
    sinon.restore()
})

describe('offerProduts unit test', () =>{

     test('should return products', async() =>{

        sinon.stub(getProducts, 'getProductsAvailable').returns([{ id: 1,name:'Arroz' },  { id: 2,name:'papa' }])

        const products = await offerProducts.offer()

        expect(products).toEqual([{ id: 1,name:'Arroz' },  { id: 2,name:'papa' }]);

     });

     test('should return empty products', async() =>{
        sinon.stub(getProducts, 'getProductsAvailable').returns(null)

        const products = await offerProducts.offer()

        expect(products).toEqual([]);

     });     


});