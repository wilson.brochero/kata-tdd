const slotRepository = require('../../../infraestructure/repository/logistic/slotRepository')
const toggle = require('../../../../src/infraestructure/toggles')


exports.getSlotsAvailableFromLogistic = async () => {
    const slotsFromRepository = await slotRepository.get()   
    return slotsFromRepository
}


