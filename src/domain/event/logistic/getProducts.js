const productRepository = require ('../../../infraestructure/repository/logistic/productRepository')

exports.getProductsAvailable = async () => {
    const productsFromRepository = await productRepository.get()   
    return productsFromRepository
}


