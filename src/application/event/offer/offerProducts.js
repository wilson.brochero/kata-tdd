const getProducts = require('../../../domain/event/logistic/getProducts')

exports.offer = async () => {
    const products = getProducts.getProductsAvailable()
    if(!products) return []
    return products
}
